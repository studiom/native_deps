#!/bin/bash

SELF_DIR=$(dirname $BASH_SOURCE[0])
source "$SELF_DIR/env"
PWD=`pwd`

OGL_DIR=bits_ogl
OGLFAT_LIB=$OGL_DIR/build/libbitsogl.a

if [ ! -d "$OGLFAT_LIB" ]; then
	git clone --recursive "git@bitbucket.org:studiom/bits_ogl.git"
fi

(cd $OGL_DIR && make) || exit 1

cp $OGLFAT_LIB libbitsogl.a
cp -r "$OGL_DIR/build/include" "$INCLUDE/ogl"

if [ "$CLEAN" = true ]; then
	rm -rf "$OGL_DIR" "$OGL_ZIP"
fi	
