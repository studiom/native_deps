#!/bin/bash

SELF_DIR=$(dirname $BASH_SOURCE[0])
source "$SELF_DIR/env"
PWD=`pwd`

CLANDMARK_ZIP="clandmark.zip"
CLANDMARK_DIR="clandmark-master"
IOS_BUILD="$CLANDMARK_DIR/build_ios"
MACOS_BUILD="$CLANDMARK_DIR/build_macos"
#-DCMAKE_FIND_FRAMEWORK=LAST - to prevent cmake picking up libpng headers from local mono installation (/Library/Frameworks/Mono.framework/Headers/libpng14/)
CMAKE_IOS_FLAGS="-DCMAKE_TOOLCHAIN_FILE=$PWD/polly/ios-9-3.cmake -DCMAKE_FIND_FRAMEWORK=LAST"
CMAKE_FLAGS="-GXcode -DCMAKE_BUILD_TYPE=Release"
XCODE_XCCONFIG_FILE="$PWD/polly/scripts/NoCodeSign.xcconfig"
XCODE_FLAGS="ONLY_ACTIVE_ARCH=NO ENABLE_BITCODE=YES"

IOS_LIB="$IOS_BUILD/libclandmark/Release-iphoneos"
SIM_LIB="$IOS_BUILD/libclandmark/Release-iphonesimulator"
MACOS_LIB="$MACOS_BUILD/libclandmark/Release"


if [ ! -f $CLANDMARK_ZIP ]; then
	curl -L -o $CLANDMARK_ZIP 'https://codeload.github.com/uricamic/clandmark/zip/73b694da245e136d0b09d43a0b17fe8677c9e55e'
fi

if [ ! -d $CLANDMARK_DIR ]; then
	tar zxf $CLANDMARK_ZIP
fi

if [ ! -d polly ]; then
	bash "$SELF_DIR/polly.sh"
fi

mkdir -p "$IOS_BUILD"
(
set -e
cd "$IOS_BUILD"	
cmake .. $CMAKE_FLAGS $CMAKE_IOS_FLAGS
xcodebuild $XCODE_FLAGS -arch armv7 -arch arm64 -target clandmark -configuration Release
xcodebuild $XCODE_FLAGS -arch armv7 -arch arm64 -target flandmark -configuration Release

xcodebuild $XCODE_FLAGS -arch i386 -arch x86_64 -target clandmark -configuration Release -sdk iphonesimulator
xcodebuild $XCODE_FLAGS -arch i386 -arch x86_64 -target flandmark -configuration Release -sdk iphonesimulator
)

lipo -create $SIM_LIB/libclandmark.1.5.dylib $IOS_LIB/libclandmark.1.5.dylib -output libclandmark.1.dylib
lipo -create $SIM_LIB/libflandmark.1.5.dylib $IOS_LIB/libflandmark.1.5.dylib -output libflandmark.1.dylib

# mkdir -p "$MACOS_BUILD"
# (
# cd "$MACOS_BUILD"	
# cmake .. $CMAKE_FLAGS
# xcodebuild -configuration Release build
# ) || exit 1
# echo `pwd`
# mkdir -p macos && cp -av "$MACOS_BUILD/dlib/Release/"* "macos/"

mkdir -p "$INCLUDE/clandmark"
cp -r "$CLANDMARK_DIR/libclandmark/"*.h "$INCLUDE/clandmark/"
cp $IOS_BUILD/CLandmarkConfig.h "$INCLUDE/clandmark/"
cp -r $CLANDMARK_DIR/3rd_party/CImg-1.5.6/* "$INCLUDE/clandmark/"
cp -r $CLANDMARK_DIR/3rd_party/rapidxml-1.13/* "$INCLUDE/clandmark/"

if [ "$CLEAN" = true ]; then
	rm -rf "$DLIB_ZIP" "$CLANDMARK_DIR"
fi	

	