#!/bin/bash

SOURCE_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source "$SOURCE_DIR/env"
DEPS_DIR="${SOURCE_DIR}/../deps"

OPENCV_ZIP=${DEPS_DIR}/opencv2.framework.zip
echo "OPENCV_ZIP=$OPENCV_ZIP"

if [ ! -f $OPENCV_ZIP ]; then
	curl -L -o $OPENCV_ZIP 'http://sourceforge.net/projects/opencvlibrary/files/opencv-ios/3.1.0/opencv2.framework.zip/download'
fi
unzip $OPENCV_ZIP

if [ "$CLEAN" = true ]; then
	rm -f $OPENCV_ZIP
fi
