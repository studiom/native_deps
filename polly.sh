#!/bin/bash

SELF_DIR=$(dirname $BASH_SOURCE[0])
source "$SELF_DIR/env"

POLLY_ZIP="polly.zip"

if [ ! -f $POLLY_ZIP ]; then
	curl -o $POLLY_ZIP "https://codeload.github.com/ruslo/polly/zip/master"
fi

unzip $POLLY_ZIP
mv polly-master polly

if [ "$CLEAN" = true ]; then
	rm -f $(POLLY_ZIP)
fi

	