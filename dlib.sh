#!/bin/bash

SELF_DIR=$(dirname $BASH_SOURCE[0])
source "$SELF_DIR/env"
PWD=`pwd`

DLIB_ZIP="dlib-19.0.tar.bz"
DLIB_DIR="dlib-19.0"
IOS_BUILD="$DLIB_DIR/build_ios"
MACOS_BUILD="$DLIB_DIR/build_macos"
#-DCMAKE_FIND_FRAMEWORK=LAST - to prevent cmake picking up libpng headers from local mono installation (/Library/Frameworks/Mono.framework/Headers/libpng14/)
CMAKE_IOS_FLAGS="-DCMAKE_TOOLCHAIN_FILE=$PWD/polly/ios-nocodesign-10-1-wo-armv7s.cmake -DCMAKE_FIND_FRAMEWORK=LAST"
CMAKE_FLAGS="-GXcode -DCMAKE_BUILD_TYPE=Release -DDLIB_NO_GUI_SUPPORT=1"
XCODE_XCCONFIG_FILE="$PWD/polly/scripts/NoCodeSign.xcconfig"
IOS_LIB="$IOS_BUILD/dlib/Release-iphoneos/libdlib.a"
SIM_LIB="$IOS_BUILD/dlib/Release-iphonesimulator/libdlib.a"
MACOS_LIB="$MACOS_BUILD/dlib/Release/libdlib.*"
if [ ! -f $DLIB_ZIP ]; then
	curl -L -o $DLIB_ZIP 'http://dlib.net/files/dlib-19.0.tar.bz2'
fi

if [ ! -d $DLIB_DIR ]; then
	tar zxf $DLIB_ZIP
fi

if [ ! -d polly ]; then
	bash "$SELF_DIR/polly.sh"
fi

mkdir -p "$IOS_BUILD"
(
cd "$IOS_BUILD"	
XCODE_XCCONFIG_FILE=$XCODE_XCCONFIG_FILE cmake .. $CMAKE_FLAGS $CMAKE_IOS_FLAGS
xcodebuild ONLY_ACTIVE_ARCH=NO IPHONEOS_DEPLOYMENT_TARGET=9.0 -arch armv7 -arch arm64 -target dlib -configuration Release build
xcodebuild ONLY_ACTIVE_ARCH=NO IPHONEOS_DEPLOYMENT_TARGET=9.0 -arch i386 -arch x86_64 -target dlib -configuration Release -sdk iphonesimulator build
) || exit 1

lipo -create $SIM_LIB $IOS_LIB -output libdlib.a

mkdir -p "$MACOS_BUILD"
(
cd "$MACOS_BUILD"	
cmake .. $CMAKE_FLAGS
xcodebuild -configuration Release build
) || exit 1
echo `pwd`
mkdir -p macos && cp -av "$MACOS_BUILD/dlib/Release/"* "macos/"

cp -r $DLIB_DIR/dlib "$INCLUDE/dlib"

if [ "$CLEAN" = true ]; then
	rm -rf "$DLIB_ZIP" "$DLIB_DIR"
fi	

	