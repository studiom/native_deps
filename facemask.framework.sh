#!/bin/bash
SOURCE="${BASH_SOURCE[0]}"
source "$(dirname $SOURCE)/env"

FM_DIR=face_mask

if [ ! -d $FM_DIR ]; then
	git clone --recursive "git@bitbucket.org:studiom/face_mask.git"
fi

(cd "$FM_DIR" && make) || exit 1

cp -r "$FM_DIR/build/facemask.framework" .
cp -r "$FM_DIR/deps/opencv2.framework" .
cp -r "$FM_DIR/deps/libbitsogl.a" .
cp -r "$FM_DIR/deps/include/"* "$INCLUDE/"

if [ "$CLEAN" = true ]; then
	rm -rf "$FM_DIR"
fi