#!/bin/bash
SOURCE="${BASH_SOURCE[0]}"
source "$(dirname $SOURCE)/env"

BOOST_LIBS="atomic chrono date_time exception filesystem program_options random signals system thread log"
BOOST_VER=1.61.0

if [ ! -f boost/boost.sh ]; then
	mkdir -p boost
	curl -L -o boost/boost.sh "https://raw.githubusercontent.com/faithfracture/Apple-Boost-BuildScript/master/boost.sh"
fi

(cd boost && bash boost.sh --boost-libs "$BOOST_LIBS" --boost-version $BOOST_VER) || exit 1
cp -r "boost/build/boost/1.61.0/ios/framework/boost.framework" .
mkdir -p macosx
cp -r "boost/build/boost/1.61.0/osx/framework/boost.framework" "macosx/"

if [ "$CLEAN" = true ]; then
	rm -rf boost
fi
	
