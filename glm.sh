#!/bin/bash

SOURCE_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source "$SOURCE_DIR/env"
DEPS_DIR="$SOURCE_DIR/../deps"

GLM_ZIP=${DEPS_DIR}/glm-0.9.7.5.zip
GLM_DIR=${GLM_ZIP%.*}
echo "source=$SOURCE_DIR"

if [ ! -d $GLM_DIR ]; then
	if [ ! -f $GLM_ZIP ]; then
		curl -L -o $GLM_ZIP "https://codeload.github.com/g-truc/glm/zip/0.9.7.5"
	fi
	unzip $GLM_ZIP
fi

rm -rf ${INCLUDE}/glm > /dev/null 2>&1
cp -r "$GLM_DIR/glm" "$INCLUDE/glm"

if [ "$CLEAN" = true ]; then
	rm -rf "$GLM_ZIP"
	rm -rf "$GLM_DIR"
fi
