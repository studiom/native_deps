#!/bin/bash

source "$(dirname $BASH_SOURCE[0])/env"
ASSIMP_ZIP="assimp-3.2.zip"
ASSIMP_DIR="${ASSIMP_ZIP%.*}"
ASSIMP_FAT_LIB="$ASSIMP_DIR/lib/iOS/libassimp-fat.a"

if [ ! -f $ASSIMP_ZIP ]; then
	curl -L -o "$ASSIMP_ZIP" "https://codeload.github.com/assimp/assimp/zip/v3.2"
fi

if [ ! -f $ASSIMP_DIR ]; then
	tar zxf "$ASSIMP_ZIP"
fi

#subshell
if [ "$HEADERS_ONLY" = false ]; then
	(
	cd "$ASSIMP_DIR/port/iOS/"
	bash ./build.sh --stdlib=libc++ --archs="armv7 arm64 i386 x86_64"	
	) || exit 1
	cp "$ASSIMP_DIR/lib/iOS/libassimp-fat.a" libassimp.a
fi

cp -r "$ASSIMP_DIR/include/assimp" "$INCLUDE/assimp"

if [ "$CLEAN" = true ]; then
	rm -rf $ASSIMP_DIR
	rm -rf $ASSIMP_ZIP
fi