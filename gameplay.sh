#!/bin/bash
SOURCE="${BASH_SOURCE[0]}"
source "$(dirname $SOURCE)/env"

GAMEPLAY_DIR="GamePlay"

GAMEPLAY_SUBDIR="gameplay"
TARGET_NAME="gameplay-ios"
BUILD_CONFIG="Release"
IOS="build/$BUILD_CONFIG-iphoneos"
SIM="build/$BUILD_CONFIG-iphonesimulator"
STATIC_LIB_NAME="libgameplay.a"
EXTERNAL_DEPS_PATH="external-deps/lib/ios"
EXTERNAL_DEPS_HEADER="external-deps/include"

if [ ! -d $GAMEPLAY_DIR ]; then
	git clone --recursive "https://github.com/gameplay3d/GamePlay.git"
fi

(cd "$GAMEPLAY_DIR" && sh install.sh) || exit 1
cd $GAMEPLAY_DIR/$GAMEPLAY_SUBDIR
xcodebuild ARCH="armv7 armv7s arm64 i386 x86_64" ONLY_ACTIVE_ARCH=NO VALID_ARCHS="x86_64 armv7s arm64 i386 i386" -target $TARGET_NAME -sdk iphonesimulator
xcodebuild -target $TARGET_NAME -sdk iphoneos
cd ..
cd ..
lipo -create $GAMEPLAY_DIR/$GAMEPLAY_SUBDIR/$IOS/$STATIC_LIB_NAME $GAMEPLAY_DIR/$GAMEPLAY_SUBDIR/$SIM/$STATIC_LIB_NAME -output $STATIC_LIB_NAME

cp -Rnv $GAMEPLAY_DIR/$GAMEPLAY_SUBDIR/src ./temp

find ./temp -name "*.cpp" -delete
find ./temp -name "*.mm" -delete

cp -Rnv temp include/$GAMEPLAY_SUBDIR
rm -rf temp

# #lipo -create $GAMEPLAY_DIR/$EXTERNAL_DEPS_PATH/arm/$EXTERNAL_DEPS_LIBNAME $GAMEPLAY_DIR/$EXTERNAL_DEPS_PATH/x86/$EXTERNAL_DEPS_LIBNAME -output $EXTERNAL_DEPS_LIBNAME
cp -Rnv $GAMEPLAY_DIR/$EXTERNAL_DEPS_PATH/arm ./arm
cp -Rnv $GAMEPLAY_DIR/$EXTERNAL_DEPS_PATH/x86 ./x86
cp -Rnv $GAMEPLAY_DIR/$EXTERNAL_DEPS_HEADER .

if [ "$CLEAN" = true ]; then
rm -rf $GAMEPLAY_DIR
fi

