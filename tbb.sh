

SELF_DIR=$(dirname $BASH_SOURCE[0])
source "$SELF_DIR/env"
PWD=`pwd`

TBB_ZIP=tbb.zip
TBB_DIR=intel_tbb_ios-master
ARM64=$TBB_DIR/build/macos_arm64_clang_cc8.0.0_ios10.1_release
ARMV7=$TBB_DIR/build/macos_armv7_clang_cc8.0.0_ios10.1_release
x86_64=$TBB_DIR/build/macos_intel64_clang_cc8.0.0_ios10.1_release
TDD_DYLIB=libtbb.dylib
TDD_MALLOC_DYLIB=libtbbmalloc.dylib
TDD_MALLOC_PROXY_DYLIB=libtbbmalloc_proxy.dylib

if [ ! -f $TBB_ZIP ]; then
	curl -L -o $TBB_ZIP 'https://codeload.github.com/studiomobile/intel_tbb_ios/zip/master'
fi

if [ ! -d $TBB_DIR ]; then
	unzip $TBB_ZIP
fi


(cd $TBB_DIR && make target=ios arch=arm64) || exit 1
(cd $TBB_DIR && make target=ios arch=armv7) || exit 1
(cd $TBB_DIR && make target=ios) || exit 1


lipo -create $ARM64/$TDD_DYLIB $ARMV7/$TDD_DYLIB $x86_64/$TDD_DYLIB -output $TDD_DYLIB
lipo -create $ARM64/$TDD_MALLOC_DYLIB $ARMV7/$TDD_MALLOC_DYLIB $x86_64/$TDD_MALLOC_DYLIB -output $TDD_MALLOC_DYLIB
lipo -create $ARM64/$TDD_MALLOC_PROXY_DYLIB $ARMV7/$TDD_MALLOC_PROXY_DYLIB $x86_64/$TDD_MALLOC_PROXY_DYLIB -output $TDD_MALLOC_PROXY_DYLIB

cp -r "$TBB_DIR/include/"* "$INCLUDE"

if [ "$CLEAN" = true ]; then
	rm -rf "$TBB_ZIP" "$TBB_DIR"
fi	
