#!/bin/bash
SOURCE="${BASH_SOURCE[0]}"
source "$(dirname $SOURCE)/env"

FW_DIR=npd_facedetect

if [ ! -d $FW_DIR ]; then
	git clone --recursive "git@bitbucket.org:studiom/npd_facedetect.git"
fi

(cd "$FW_DIR" && make) || exit 1

cp -r "$FW_DIR/npd_facedetect/build/npd_facedetect.framework" .
cp -r "$FW_DIR/npd_facedetect/build/model.npd" .
cp -r "$FW_DIR/deps/boost.framework" .
mkdir -p macosx
cp -r "$FW_DIR/deps/macosx/boost.framework" macosx

if [ "$CLEAN" = true ]; then
	rm -rf "$FW_DIR"
fi